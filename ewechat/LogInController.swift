//
//  LogInController.swift
//  ewechat
//
//  Created by Damjan Ivanovic on 6/9/17.
//  Copyright © 2017 Damjan Ivanovic. All rights reserved.
//

import UIKit
import Firebase

class LogInController: UIViewController {
    
    var messagesControler: MessagesController?

    let inputsContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 15
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let loginRegisterButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor(r: 50, g:50 , b:50)
        button.setTitle("Register", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 15
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        
        button.addTarget(self, action: #selector(handleLoginRegister), for: .touchUpInside)
        
        return button
    }()    
    
    let nameTextFild: UITextField = {
        let ntf = UITextField()
        ntf.autocorrectionType = .no
        ntf.placeholder = "Name"
        ntf.translatesAutoresizingMaskIntoConstraints = false
        return ntf
    }()
    
    let nameLineSeparator: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 200, g: 200, b: 200)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let emailTextFild: UITextField = {
        let ntf = UITextField()
        ntf.autocorrectionType = .no
        ntf.placeholder = "Email addrese"
        ntf.translatesAutoresizingMaskIntoConstraints = false
        return ntf
    }()
    
    let emailLineSeparator: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 200, g: 200, b: 200)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let passwordTextFild: UITextField = {
        let ntf = UITextField()
        ntf.placeholder = "Password"
        ntf.autocorrectionType = .no
        ntf.translatesAutoresizingMaskIntoConstraints = false
        ntf.isSecureTextEntry = true
        return ntf
    }()
    
    lazy var loginImageView: UIImageView = {
        let loginImage = UIImageView()
        loginImage.image = UIImage(named: "Ewebeli")
        loginImage.translatesAutoresizingMaskIntoConstraints = false
        loginImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleSelectProfileImageView)))
        loginImage.isUserInteractionEnabled = true
        return loginImage
    }()
   
    
    lazy var loginRegisterSegmentedContrrol: UISegmentedControl = {
        let loginRegister = UISegmentedControl(items: ["Login" , "Register"])
        loginRegister.translatesAutoresizingMaskIntoConstraints = false
        loginRegister.tintColor = UIColor.white
        loginRegister.selectedSegmentIndex = 1
        loginRegister.addTarget(self, action: #selector(handleLoginRegisterChange), for: .valueChanged)
        return loginRegister
    }()
    
    func handleLoginRegisterChange() {
        let title = loginRegisterSegmentedContrrol.titleForSegment(at: loginRegisterSegmentedContrrol.selectedSegmentIndex)
        loginRegisterButton.setTitle(title, for: .normal)
        
        // change input container for login and register
        inputContainerViewHeightAnchor?.constant = loginRegisterSegmentedContrrol.selectedSegmentIndex == 0 ? 100 : 150
        
        //change of text filds for login and register
        nameTextFildHeightAnchor?.isActive = false
        nameTextFildHeightAnchor = nameTextFild.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor, multiplier: loginRegisterSegmentedContrrol.selectedSegmentIndex == 0 ? 0 : 1/3)
        nameTextFildHeightAnchor?.isActive = true
        nameTextFild.layer.masksToBounds = true
        
        emailTextFildHeightAnchor?.isActive = false
        emailTextFildHeightAnchor = emailTextFild.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor, multiplier: loginRegisterSegmentedContrrol.selectedSegmentIndex == 0 ? 1/2 : 1/3)
        emailTextFildHeightAnchor?.isActive = true
        
        passwordTextFildHeightAnchor?.isActive = false
        passwordTextFildHeightAnchor = passwordTextFild.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor, multiplier: loginRegisterSegmentedContrrol.selectedSegmentIndex == 0 ? 1/2 : 1/3)
        passwordTextFildHeightAnchor?.isActive = true
        
        
        //change of name line separator when hit login or register
        nameLineSeparatorHeightAnchor?.constant = loginRegisterSegmentedContrrol.selectedSegmentIndex == 0 ? 0 : 1
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor(r: 5, g: 5, b: 5)
        
        view.addSubview(inputsContainerView)
        view.addSubview(loginRegisterButton)
        view.addSubview(loginImageView)
        view.addSubview(loginRegisterSegmentedContrrol)
        
        setupInputContainerView()
        setupLoginRegisterButton()
        setupLoginImageView()
        setupLoginRegisterSegmentedContrrol()
    }
    
    func setupLoginRegisterSegmentedContrrol() {
        loginRegisterSegmentedContrrol.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loginRegisterSegmentedContrrol.bottomAnchor.constraint(equalTo: inputsContainerView.topAnchor , constant: -12).isActive = true
        loginRegisterSegmentedContrrol.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        loginRegisterSegmentedContrrol.heightAnchor.constraint(equalToConstant: 35).isActive = true
    }
    
    func setupLoginImageView() {
        loginImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loginImageView.bottomAnchor.constraint(equalTo: loginRegisterSegmentedContrrol.topAnchor , constant: -12).isActive = true
        loginImageView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        loginImageView.heightAnchor.constraint(equalToConstant: 170).isActive = true
    }
    
    var inputContainerViewHeightAnchor: NSLayoutConstraint?
    var nameTextFildHeightAnchor: NSLayoutConstraint?
    var emailTextFildHeightAnchor: NSLayoutConstraint?
    var passwordTextFildHeightAnchor: NSLayoutConstraint?
    var nameLineSeparatorHeightAnchor: NSLayoutConstraint?
    
    func setupInputContainerView() {
    
        inputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        inputsContainerView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        inputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        inputContainerViewHeightAnchor = inputsContainerView.heightAnchor.constraint(equalToConstant: 150)
            inputContainerViewHeightAnchor?.isActive = true
        
        
        inputsContainerView.addSubview(nameTextFild)
        inputsContainerView.addSubview(nameLineSeparator)
        inputsContainerView.addSubview(emailTextFild)
        inputsContainerView.addSubview(emailLineSeparator)
        inputsContainerView.addSubview(passwordTextFild)
        
        func setupNameTextFild() {
            
            nameTextFild.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor , constant: 12).isActive = true
            nameTextFild.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
            nameTextFild.topAnchor.constraint(equalTo: inputsContainerView.topAnchor).isActive = true
            
            nameTextFildHeightAnchor = nameTextFild.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor, multiplier: 1/3)
            nameTextFildHeightAnchor?.isActive = true
        }
        
        func setupNameLineSeparator() {
            nameLineSeparator.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
            nameLineSeparator.topAnchor.constraint(equalTo: nameTextFild.bottomAnchor).isActive = true
            nameLineSeparator.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
            nameLineSeparatorHeightAnchor = nameLineSeparator.heightAnchor.constraint(equalToConstant: 1)
                nameLineSeparatorHeightAnchor?.isActive = true
        }
        
        func setupEmailTextFild() {
            emailTextFild.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor, constant: 12).isActive = true
            emailTextFild.topAnchor.constraint(equalTo: nameLineSeparator.bottomAnchor).isActive = true
            emailTextFild.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
            
            emailTextFildHeightAnchor = emailTextFild.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor, multiplier: 1/3)
            emailTextFildHeightAnchor?.isActive = true
        }
        
        func setupEmailLineSeparator() {
            emailLineSeparator.heightAnchor.constraint(equalToConstant: 1).isActive = true
            emailLineSeparator.topAnchor.constraint(equalTo: emailTextFild.bottomAnchor).isActive = true
            emailLineSeparator.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
            emailLineSeparator.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        }
        func setupPasswordTextFild() {
            passwordTextFild.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor , constant: 12).isActive = true
            passwordTextFild.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
            passwordTextFildHeightAnchor = passwordTextFild.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor, multiplier: 1/3)
                    passwordTextFildHeightAnchor?.isActive = true
            
            passwordTextFild.topAnchor.constraint(equalTo: emailLineSeparator.bottomAnchor).isActive = true
        }
        
        setupNameTextFild()
        setupNameLineSeparator()
        setupEmailTextFild()
        setupEmailLineSeparator()
        setupPasswordTextFild()
    }
    
    func setupLoginRegisterButton() {
        loginRegisterButton.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        loginRegisterButton.topAnchor.constraint(equalTo: inputsContainerView.bottomAnchor , constant: 12).isActive = true
        loginRegisterButton.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        loginRegisterButton.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
        
    }
}
 
    extension UIColor {
        convenience init (r: CGFloat , g: CGFloat , b: CGFloat) {
        self.init(red: r/255 , green: g/255 , blue: b/255 , alpha: 1)
        }
    }

