//
//  Extensions.swift
//  ewechat
//
//  Created by Damjan Ivanovic on 6/24/17.
//  Copyright © 2017 Damjan Ivanovic. All rights reserved.
//

import UIKit


let imageCache = NSCache<NSString, UIImage>()

extension UIImageView {
    
    func loadImageUsingCacheWithUrlString(urlString: String) {
        
        self.image = nil
        
        //check cache for image
        if let cachedImage = imageCache.object(forKey: urlString as NSString) {
            self.image = cachedImage
            return
        }
        
        //download image if there is new one
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
    
            if error != nil {
                print(error!)
            }
            DispatchQueue.main.async(execute: {
                
                if let downoadedimage = UIImage (data: (data)!) {
                      imageCache.setObject(downoadedimage, forKey: urlString as NSString)
                self.image = downoadedimage
                }
            })
        }).resume()
    
    }
}
