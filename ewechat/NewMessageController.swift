//
//  NewMessageController.swift
//  ewechat
//
//  Created by Damjan Ivanovic on 6/21/17.
//  Copyright © 2017 Damjan Ivanovic. All rights reserved.
//

import UIKit
import Firebase

class NewMessageController: UITableViewController {
    
    let cellId = "cellId"
    
    var users = [MemberUser]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cencel", style: .plain, target: self , action: #selector(handleCancel))
        
        tableView.register(UserCell.self, forCellReuseIdentifier: cellId)
        
        fetchUser()
        
    }

    
    //fetch users from firebase tree
    func fetchUser() {
        Database.database().reference().child("users").observe(.childAdded, with: { (snapshot) in
            
            if let dictionary = snapshot.value as? [String:AnyObject] {
                let user = MemberUser()
                user.id = snapshot.key
                user.setValuesForKeys(dictionary)
                self.users.append(user)
                
        
                    DispatchQueue.main.async(execute: { 
                        self.tableView.reloadData()
                    })
            }
            
        
        }, withCancel: nil)
    }
    
    func handleCancel() {
        dismiss(animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellId)
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)as! UserCell
        
        let user = users[indexPath.row]
        
        cell.textLabel?.text = user.name
        cell.detailTextLabel?.text = user.email
      
        
        if let profileImageURL = user.profileImageURL {
            cell.profileImageView.loadImageUsingCacheWithUrlString(urlString: profileImageURL)
        }
        
        return cell
        
             }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    var messagesControler: MessagesController?
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true) {
            print("Dismis complited")
            let user = self.users[indexPath.row]
            self.messagesControler?.showChatControllerForUser(user: user)
        }
    }
}




