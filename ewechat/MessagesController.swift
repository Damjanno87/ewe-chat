//
//  ViewController.swift
//  ewechat
//
//  Created by Damjan Ivanovic on 6/17/17.
//  Copyright © 2017 Damjan Ivanovic. All rights reserved.
//

import UIKit
import Firebase

class MessagesController: UITableViewController {
    
    let cellId = "cellId"
    

    override func viewDidLoad() {
        super.viewDidLoad()

//        var ref: DatabaseReference!
        
//        ref = Database.database().reference(fromURL: "https://ewe-chat-32f47.firebaseio.com/")
//        ref.updateChildValues(["something": 4321])
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(handleLogout))
        
        let image = UIImage(named: "speechbeli")
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(handleNewMessage))
        
        checkIfUserIsLoggedin()
        
        tableView.register(UserCell.self, forCellReuseIdentifier: cellId)
        
//        observeMessages()
       
        
    }
    
    var messages = [Message]()
    var messagesDictionary = [String : Message]()
    
    
    func observeUserMessages() {
        
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        let ref = Database.database().reference().child("user-messages").child(uid)
        ref.observe(.childAdded, with: { (snapshot) in
         
            let messageId = snapshot.key
            let messageReference = Database.database().reference().child("messages").child(messageId)
            
            messageReference.observeSingleEvent(of: .value, with: { (snapshot) in
                
                if let dictionary = snapshot.value as? [String: AnyObject] {
                    let message = Message()
                    message.setValuesForKeys(dictionary)
                    //                self.messages.append(message)
                    
                    if let chatPartnerId = message.chatPartnerId() {
                        self.messagesDictionary[chatPartnerId] = message
                        
                        self.messages = Array(self.messagesDictionary.values)
                        self.messages.sort(by: { (m1, m2) -> Bool in
                            return (m1.timestamp?.intValue)! > (m2.timestamp?.intValue)!
                        })
                    }
                    
                    self.timer?.invalidate()
                    self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.handleReloadTable), userInfo: nil, repeats: false)
                    //                print(message.text as Any)
                
                }
                
            }, withCancel: nil)
            
        }, withCancel: nil)
        
        
    }
    
    var timer: Timer?
    
    func handleReloadTable() {
        DispatchQueue.main.async(execute: {
            self.tableView.reloadData() //tis will chras because of becround tread so we call row abowe dispach
        })
    }
    
//    func observeMessages() {
//        let ref = Database.database().reference().child("messages")
//        ref.observe(.childAdded, with: { (snapshot) in
//            if let dictionary = snapshot.value as? [String: AnyObject] {
//                  let message = Message()
//                message.setValuesForKeys(dictionary)
////                self.messages.append(message)
//                
//                if let toId = message.toId {
//                    self.messagesDictionary[toId] = message
//                    
//                    self.messages = Array(self.messagesDictionary.values)
//                    self.messages.sort(by: { (m1, m2) -> Bool in
//                        return (m1.timestamp?.intValue)! > (m2.timestamp?.intValue)!
//                    })
//                }
////                print(message.text as Any)
//                DispatchQueue.main.async(execute: {
//                    self.tableView.reloadData() //tis will chras because of becround tread so we call row abowe dispach
//                })
//            }
//          
//        }, withCancel: nil)
//    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! UserCell
        
        let message = messages[indexPath.row]
        cell.message = message
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let message = messages[indexPath.row]
        
        guard let chatPartnerId = message.chatPartnerId() else {
            return
        }
        
        let ref = Database.database().reference().child("users").child(chatPartnerId)
        ref.observeSingleEvent(of: .value, with: { (snapshot1) in
            guard let dictionary = snapshot1.value as? [String:AnyObject]
                else {
                return
            }
            let user = MemberUser()
            user.id = chatPartnerId
            user.setValuesForKeys(dictionary)
            self.showChatControllerForUser(user: user) 
        }, withCancel: nil)
        
        print(message.text as Any, message.toId as Any, message.fromId as Any)
        
      
        }


    
    func handleNewMessage() {
        let newMessageController = NewMessageController()
        newMessageController.messagesControler = self
        let navController = UINavigationController(rootViewController: newMessageController)
        present(navController, animated: true, completion: nil)
    }
    
    
        //user is not logged in
    
    func checkIfUserIsLoggedin() {
        if Auth.auth().currentUser?.uid == nil {
            perform(#selector(handleLogout), with: nil, afterDelay: 0)
        } else {
           fetchUserAndSetNavBarTitle()
        }
}
//end - for name in menu
    
    func fetchUserAndSetNavBarTitle() {
        //make visible name of user in navigation menu
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        Database.database().reference().child("users").child(uid).observeSingleEvent(of:.value, with: { (Snapshot) in
            
            if let dictionary = Snapshot.value as? [String: AnyObject] {
//                self.navigationItem.title = dictionary ["name"] as? String
            
                let user = MemberUser()
                user.setValuesForKeys(dictionary)
                self.setupNavBarWithUser(user: user)
            }
            
        }, withCancel: nil)
    }
    
    func setupNavBarWithUser(user: MemberUser) {
        
        messages.removeAll()
        messagesDictionary.removeAll()
        tableView.reloadData()
        
         observeUserMessages()
        
        let titleView = UIView()
        
        titleView.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
//        titleView.backgroundColor = UIColor.red
        
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        titleView.addSubview(containerView)
        
        let profileImageView = UIImageView()
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.layer.cornerRadius = 20
        profileImageView.clipsToBounds = true
        if let profileImageURL = user.profileImageURL {
        profileImageView.loadImageUsingCacheWithUrlString(urlString: profileImageURL)
        }
        
        containerView.addSubview(profileImageView)
        
        profileImageView.leftAnchor.constraint(equalTo:  containerView.leftAnchor).isActive = true
        profileImageView.centerYAnchor.constraint(equalTo:  containerView.centerYAnchor).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 40).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        
        let nameLable = UILabel()
        containerView.addSubview(nameLable)

        nameLable.text = user.name
        nameLable.translatesAutoresizingMaskIntoConstraints = false
        
        nameLable.leftAnchor.constraint(equalTo: profileImageView.rightAnchor , constant: 8).isActive = true
        nameLable.centerYAnchor.constraint(equalTo: profileImageView.centerYAnchor).isActive = true
        nameLable.rightAnchor.constraint(equalTo:  containerView.rightAnchor).isActive = true
        nameLable.heightAnchor.constraint(equalTo: profileImageView.heightAnchor).isActive = true
        
        
        containerView.centerXAnchor.constraint(equalTo: titleView.centerXAnchor).isActive = true
        containerView.centerYAnchor.constraint(equalTo: titleView.centerYAnchor).isActive = true
        
        self.navigationItem.titleView = titleView
        
//        titleView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showChatController)))
    }

    func showChatControllerForUser(user: MemberUser) {
        let chatLogController = ChatLogController(collectionViewLayout: UICollectionViewFlowLayout())
        chatLogController.user = user
        navigationController?.pushViewController(chatLogController, animated: true)
    }
    
    func handleLogout() {
        do {
            try Auth.auth().signOut()
        } catch let logoutError {
            print(logoutError)
        }
        
        let loginController = LogInController()
        loginController.messagesControler = self
        present(loginController, animated: true, completion: nil)
    } 
}
