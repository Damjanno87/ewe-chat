//
//  UserCell.swift
//  ewechat
//
//  Created by Damjan Ivanovic on 8/9/17.
//  Copyright © 2017 Damjan Ivanovic. All rights reserved.
//

import UIKit
import Firebase

class UserCell: UITableViewCell {
    
    
    var message: Message? {
        didSet {
            
            setUpNameAndProfileImage()
            
            detailTextLabel?.text = message?.text
            

            if let seconds = message?.timestamp?.doubleValue {
                let timesampDate = NSDate(timeIntervalSince1970: seconds)
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "hh:mm:ss a"
                timeLabel.text = dateFormatter.string(from: timesampDate as Date) 
            }
        }
    }

    private func setUpNameAndProfileImage (){
        
                    if let id = message?.chatPartnerId() {
                        let ref = Database.database().reference().child("users").child(id)
                        ref.observeSingleEvent(of: .value, with: { (snapshot) in
                            if let dictionary = snapshot.value as? [String:AnyObject] {
                                self.textLabel?.text = dictionary["name"] as? String
        
                                if let profileImageURL = dictionary["profileImageURL"] as? String {
                                    self.profileImageView.loadImageUsingCacheWithUrlString(urlString: profileImageURL)
                                }
                            } 
                        }, withCancel: nil)
                    }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        textLabel?.frame = CGRect(x: 74, y: textLabel!.frame.origin.y, width: textLabel!.frame.width, height: textLabel!.frame.height)
        
        detailTextLabel?.frame = CGRect(x: 74, y: detailTextLabel!.frame.origin.y, width:detailTextLabel!.frame.width, height: detailTextLabel!.frame.height)
    }
    
    var profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 28
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    var timeLabel: UILabel = {
        let label = UILabel()
//        label.text = "HH:MM:SS"
        label.font = UIFont.systemFont(ofSize: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.lightGray
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        
        addSubview(profileImageView)
        addSubview(timeLabel)
        
        profileImageView.leftAnchor.constraint(equalTo: self.leftAnchor , constant: 8).isActive = true
        profileImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 56).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: 56).isActive = true
        
        timeLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 8).isActive = true
        timeLabel.topAnchor.constraint(equalTo: self.topAnchor , constant: 22).isActive = true
        timeLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        timeLabel.heightAnchor.constraint(equalTo: (textLabel?.heightAnchor)!).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
