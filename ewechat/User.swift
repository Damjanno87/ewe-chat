//
//  User.swift
//  ewechat
//
//  Created by Damjan Ivanovic on 6/24/17.
//  Copyright © 2017 Damjan Ivanovic. All rights reserved.
//

import UIKit

class MemberUser: NSObject {
//    var user: String?
    var id: String?
    var name: String?
    var email: String?
    var profileImageURL: String?
}
