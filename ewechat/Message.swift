
//  Message.swift
//  ewechat
//
//  Created by Damjan Ivanovic on 8/8/17.
//  Copyright © 2017 Damjan Ivanovic. All rights reserved.
//

import UIKit
import Firebase

class Message: NSObject {
    var fromId: String?
    var text: String?
    var timestamp: NSNumber?
    var toId: String?


    func chatPartnerId() -> String? {
        
        return fromId == Auth.auth().currentUser?.uid ? toId : fromId
    }
}
