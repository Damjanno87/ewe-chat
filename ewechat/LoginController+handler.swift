//
//  LoginController+handler.swift
//  ewechat
//
//  Created by Damjan Ivanovic on 6/24/17.
//  Copyright © 2017 Damjan Ivanovic. All rights reserved.
//

import UIKit
import Firebase

extension LogInController: UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    
    func handleLoginRegister() {
        if loginRegisterSegmentedContrrol.selectedSegmentIndex == 0 {
            handleLogin()
        } else {
            handleRegisterButton()
        }
    }
    
    func handleLogin() {
        guard let email = emailTextFild.text , let password = passwordTextFild.text else {
            print("Form is not valid") 
            return
        }
        Auth.auth().signIn(withEmail: email , password: password , completion: { (user: User? , error ) in
            
            if error != nil {
                print("error")
                return
            }
            
            self.messagesControler?.fetchUserAndSetNavBarTitle()
            //successfully logged in
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    //   Authentication of email on firebase
    func handleRegisterButton() {
        guard let email = emailTextFild.text , let password = passwordTextFild.text , let name = nameTextFild.text else {
            print("Form is not valid")
            return
        }
        
        Auth.auth().createUser(withEmail: email, password: password) { (user: User? , error ) in
            
            if error != nil {
                print("error")
                return
            }
            
//            var ref: DatabaseReference!
            
            guard let uid = user?.uid else {
                return
            }
            
            let imageName = NSUUID().uuidString //this represent garanter unique string every time
            let storageRef = Storage.storage().reference().child("profile_image").child("\(imageName).jpg")
            
            if let profileImage = self.loginImageView.image , let uploadData = UIImageJPEGRepresentation(profileImage, 0.1) {
                
                //using comprested image
                
//            if let uploadData = UIImagePNGRepresentation(self.loginImageView.image!) {
            
                storageRef.putData(uploadData, metadata: nil, completion: { (metadata, error) in
                   
                    if error != nil {
                        print(error!)
                        return
                    }
                
                    if let profileImageURL = metadata?.downloadURL()?.absoluteString {
                        let values = ["name": name , "email": email , "profileImageURL": profileImageURL]
                    
                    self.registerUserToDatabaseWithUID(uid: uid, values: values as [String : AnyObject])
                    }
                })
            }
            
        }
    }    // end of Authentication of email
    
    private func registerUserToDatabaseWithUID(uid: String , values: [String:AnyObject]) {
//        var ref: DatabaseReference!
        let ref = Database.database().reference()
        let usersReference = ref.child("users").child(uid)
        
        usersReference.updateChildValues(values, withCompletionBlock: { (err , ref) in
            
            if err != nil {
                print(err!)
                return
            }
            
//            self.messagesControler?.fetchUserAndSetNavBarTitle()
//            self.messagesControler?.navigationItem.title = values ["name"]as? String
            let user = MemberUser()
            user.setValuesForKeys(values)
            self.messagesControler?.setupNavBarWithUser(user: user)
            
            self.dismiss(animated: true, completion: nil) //it will go to other screen
        })
    }
    
    func handleSelectProfileImageView() {
        let imagePicker = UIImagePickerController()
        
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        
        present(imagePicker, animated: true, completion: nil)

    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var selectedImageFromPicker = UIImage?.none
        
        if let editedImage = info ["UIImagePickerControllerEditedImage"] {
            selectedImageFromPicker = editedImage as? UIImage
      
        } else if let originalImage = info ["UIImagePickerControllerOriginalImage"] {
            selectedImageFromPicker = originalImage as? UIImage
        }   //geting image out of the picker
        
        if let selectedImage = selectedImageFromPicker {
            loginImageView.image = selectedImage
        }
        
        dismiss(animated: true, completion: nil)
        
        print(info)
    }

    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print(123)
    dismiss(animated: true, completion: nil)
    }
    

}
